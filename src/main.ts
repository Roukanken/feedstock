import { defineCustomElement } from "vue";
import ExampleComponent from "./components/ExampleComponent.ce.vue";

const ExampleElement = defineCustomElement(ExampleComponent);

customElements.define("feedstock-example", ExampleElement);
