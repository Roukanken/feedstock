# Feedstock

### Including feedstock on your website:

Run `npm build` and grab `dist/feedstock.umd.js` file. Then on your website include these scripts (while replacing `path/to/` with your path)

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/3.2.31/vue.global.min.js"></script>
    <script type="module" src="path/to/feedstock.umd.js"></script>

### Currently supported custom elements

- `<feedstock-examle/>` - simple proof of concept
