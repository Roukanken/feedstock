import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import * as path from "path";

export default defineConfig({
    plugins: [vue({ customElement: true })],
    build: {
        lib: {
            entry: path.resolve(__dirname, "src/main.ts"),
            name: "Feedstock",
            fileName: (format) => `feedstock.${format}.js`,
        },
        rollupOptions: {
            external: ["vue"],
            output: {
                globals: {
                    vue: "Vue",
                },
            },
        },
    },
});
